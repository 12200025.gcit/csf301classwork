
import { StyleSheet, Text, View } from 'react-native';
import ProductContainer from './screens/Product/ProductContainer';
import Header from './Shared/Header';

export default function App() {
  return (
    <View style={styles.container}>
      <Header/>
      <ProductContainer/>
     
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop:50,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
