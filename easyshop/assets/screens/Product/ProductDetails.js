import { useEffect } from "react";
import { Text,View, Image } from "react-native";

function ProductDetails({route}) {
  return (
    <>
        <View >
            <View >
            <Image  source={{uri: route.params.image ? route.params.image : "https://cdn.pixabay.com/photo/2012/04/01/19/52/magnifying-24311_960_720.png"}}/>
            </View>
            <Text >{route.params.brand}</Text>
            <Text >Nu.{route.params.price.toFixed(2)}</Text>
           
            
            
        </View>
    </>
  );
}



export default ProductDetails;
