import { TouchableOpacity } from "react-native";
import {Text, Dimensions, StyleSheet} from "react-native";
import ProductCard from "./ProductCard";
import { useNavigation } from "@react-navigation/native";

const {width} = Dimensions.get("window");
console.log(width);

export default function ProductList({product}){
    const navigation = useNavigation();
        return( <TouchableOpacity style={style.body} onPress={() => navigation.navigate('ProductDetails', product)}>
            <ProductCard product={product}/>
        </TouchableOpacity>)
}

const style = StyleSheet.create({
    rootContainer:{
        width: '50%',
    },
    body:{
        width: width/2,
        
        
    }
})