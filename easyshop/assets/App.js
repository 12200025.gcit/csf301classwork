
import { StyleSheet, Text, View } from 'react-native';
import ProductContainer from './screens/Product/ProductContainer';
import Header from './Shared/Header';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { SafeAreaView } from "react-native-safe-area-context";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import ProductDetails from "./screens/Product/ProductDetails";
import TestScreen from "./screens/Product/TestScreen";
import { StatusBar } from "expo-status-bar";
import Icon from "react-native-vector-icons/MaterialIcons";

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();



export default function App() {
  function StackNavigation(){
    return(
      <Stack.Navigator
            screenOptions={{
              headerTitleAlign: "start",
              headerTintColor: "black",
              contentStyle: {
                backgroundColor: "orange",
              },
              headerStyle: {
                backgroundColor: "#9ab9d7",
              },
            }}
            initialRouteName="ProductOverview"
          >
            <Stack.Screen name="TestScreen" component={TestScreen}/>
            <Stack.Screen
              name="ProductOverview"
              component={ProductContainer}
              options={{
                title: "Product Overview",
              }}
            />
            <Stack.Screen name="ProductDetails" component={ProductDetails} />
  
            
          </Stack.Navigator>
     
    )
  }
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar style="auto" />
      <Header />
      <NavigationContainer>
      

<Tab.Navigator>
  
        <Tab.Screen
          name="Home"
          component={ProductContainer}
          options={{
            headerTitle: "Product Container",
            tabBarIcon: ({ color, size }) => (
              <Icon name="home" color={color} size={size} />
            ),
          }}
        />
        <Tab.Screen
          name="Setting"
          component={TestScreen}
          options={{
            tabBarIcon: ({ color, size }) => (
              <Icon name="settings" color={color} size={size} />
            ),
          }}
        />
        <Tab.Screen
          name="ProductContainer"
          component={StackNavigation}
          options={{
            tabBarIcon: ({ color, size }) => (
              <Icon name="settings" color={color} size={size} />
            ), headerShown: false
          }}
        />
        
      </Tab.Navigator>

        
      </NavigationContainer>
    </SafeAreaView>
  );

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // marginTop:50,
    // alignItems: 'center',
    // justifyContent: 'center',
  },
});
