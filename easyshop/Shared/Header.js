import { SafeAreaView, StyleSheet, Image, View } from "react-native"

export default function Header(){
    return(
        <SafeAreaView style={styles.Header}>
            <View style={styles.imageContainer}>
            <Image source={require('../assets/Logo.png')}
            resizeMode='contain'
            style={styles.image}
            />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    Header:{
        width: '100%',
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'center',
        padding: 20,
        marginTop: 80 // Todo: Delete
    },
    image:{
        width:'100%',
        height:'100%'
    }, 
    imageContainer:{
        width:'100%',
        height: 80,
        alignContent: 'center',
        alignItems: 'center',
        marginTop:0

    }
})