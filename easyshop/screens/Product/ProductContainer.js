import { useEffect, useState } from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import ProductList from './ProductList';

const  data=require("../../assets/data/products.json");
export default function ProductContainer(){
    const [products,setProducts]= useState();
    useEffect(()=>{
        setProducts(data);
        return ()=>{
            setProducts([]);
        }
    })
    return(
        <View style={{marginTop:40,backgroundColor: 'grey' }}>
        <Text>
        Product Container
        </Text>
        <FlatList data={products}
                    numColumns={2} 
                  renderItem={({item})=><ProductList product={item}/>}
                  keyExtractor={(item)=> item._id.$oid} 
                  />
    </View>
    )

}
