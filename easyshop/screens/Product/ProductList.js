import { TouchableOpacity } from "react-native";
import {Text, Dimensions, StyleSheet} from "react-native";
import ProductCard from "./ProductCard";

const {width} = Dimensions.get("window");
console.log(width)

export default function ProductList({product}){
        return( <TouchableOpacity style={style.body}>
            <ProductCard product={product}/>
        </TouchableOpacity>)
}

const style = StyleSheet.create({
    rootContainer:{
        width: '50%',
    },
    body:{
        width: width/2,
        
        
    }
})