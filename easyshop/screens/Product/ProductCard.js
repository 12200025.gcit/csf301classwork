import { View, Text, StyleSheet, Dimensions, Button, Image } from "react-native";

const {width} = Dimensions.get('window')

export default function ProductCard({product}){
    return(
        <View style={styles.rootContainer}>
            <View style={styles.imageContainer}>
            <Image style={styles.image} source={{uri: product.image ? product.image : "https://cdn.pixabay.com/photo/2012/04/01/19/52/magnifying-24311_960_720.png"}}/>
            </View>
            <Text style={styles.title}>{product.brand}</Text>
            <Text style={styles.price}>Nu.{product.price.toFixed(2)}</Text>
            {product.countInStock > 0 ? <Button title="ADD" color={'green'}/> : <Text style={styles.statusText}>Currently Not Available</Text>}
            
            
        </View>
    )
}

const styles = StyleSheet.create({
    rootContainer: {
        width: width/2 - 20,
        height: width/1.7,
        backgroundColor: 'white',
        padding: 10,
        marginBottom:20,
        marginTop: 60,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 10,
        borderRadius: 20
    },
    title:{
        fontWeight:'bold',
        fontSize: 12,
        marginTop:140

    },
    price:{
        color:'orange',
        fontSize: 18
    },
    image:{
        width: '100%',
        height: '100%',

    },
    imageContainer:{
        width: width/2-20-20,
        height: width/2 -20,
        top: -45,
        position: 'absolute',
        borderRadius: 20,
        overflow: 'hidden'
         
    },
    statusText:{
        color:'red'
    }
    
})